# quoridor-S6

Projet de programmation dans le cadre de notre troisième année de licence informatique à Montpellier.
Le but étant de développer différents algorithmes d'intelligence artificielles pour le jeu du Quoridor. 

WebSite : https://baubertin.com/quoridor/

## Structure des diférentes branches : 

# [IG](https://gitlab.com/baptiste-aubertin/quoridor-s6/-/tree/IG) : 
    La branche IG contient le code coté client (graphique, interface) délivré par notre serveur. 

# [Serveur-Node](https://gitlab.com/baptiste-aubertin/quoridor-s6/-/tree/Serveur-Node)
    La branche Serveur-Node contient le coté coté serveur (IAs, API, Reseaux, etc...) exècuté sur notre machine.

# [Elo](https://gitlab.com/baptiste-aubertin/quoridor-s6/-/tree/Elo)
    La branche Elo contient un script js permettant de tester les performances de nos IA en calculant leurs Elo.

# [Python](https://gitlab.com/baptiste-aubertin/quoridor-s6/-/tree/Python)
    La branche Python contient notre première implémentation en python du jeu sans le coté client serveur.

