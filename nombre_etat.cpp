#include <iostream>
#include <vector>
#include <cmath>
#include <array>
#include <algorithm>
#include <numeric>

#include <boost/multiprecision/cpp_int.hpp>
using ll = boost::multiprecision::cpp_int;

ll solve(int N) {
    std::vector<std::vector<std::array<ll, 2>>> dp(N * N, std::vector<std::array<ll, 2>>(1ll << N));

    dp[0][1ll << (N - 1)][0] = 1; // |
    dp[0][0][1] = 1; //  -
    dp[0][0][0] = 1; // " " 

    for(int y = 0; y < N; y++) for(int x = 0; x < N; x++) {
        if(y == N - 1 && x == N - 1)
            break;
        auto& dp1 = dp[y * N + x];
        auto& dp2 = dp[y * N + x + 1];

        for(int bit = 0; bit < (1 << N); bit++) for(int before_H : {0, 1}) {
            // pas un mur horizontal 
            dp2[bit >> 1][0] += dp1[bit][before_H];
            // mur horizontal ou pas de case a droite
            if(x == N - 1 || before_H == 0)
                dp2[bit >> 1][1] += dp1[bit][before_H];
            // mur vertical += non mur vertical N mur avant
            if((bit & 1) == 0)
                dp2[bit >> 1 | 1 << (N - 1)][0] += dp1[bit][before_H];
        }
    }
    
    ll ans = 0;
    for (int y = 0; y < (1 << N); ++y)
        for (int b : {0, 1})
            ans += dp[N * N - 1][y][b];
    return ans;
}

int main(){
    for (int n = 1; n <= 15; ++n)
        std::cout << "N = " << n << " -> " << solve(n) << " etats possibles" << std::endl;
}
